<!DOCTYPE html>
<!--
Area do Administrador
autor: Silvarney Henrique
-->
<?php
session_start(); //start a sessao iniciada na autenticação
require ("../controle/protecao.php"); //link para utilizacao das funcoes dentro de do arquivo "protecao"
protegerAdm(); //funcao que testa se o usuario logado tem autorizacao para acessar essa pagina
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>Bem Vindo, Adm!</h1>
    </body>
</html>

<!--
Pagina inicial do administrador de dentro pasta "adm". 
-->
