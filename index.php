<!DOCTYPE html>
<!--
Pagina inicial publica
autor: Silvarney Henrique
-->

<html>
    <head>
        <meta charset="UTF-8">
        <title>Pagina Inicial</title>
    </head>
    <body>
        <!-- Formulario para login: envia os dados para a autenticacao.php-->
        <form id="login" name="login" method="post" action="controle/autenticacao.php">
            <fieldset>
                <legend>Login:</legend>
                <input type="text" name="nome" placeholder="seu nome"><br>
                <input type="text" name="senha" placeholder="sua senha"><br>
                <input type="submit" name="submit" value="Entrar"><br>
                <a href="cadastro.php">Cadastrar novo usurário</a>
            </fieldset>
        </form>

        <?php
        // $user = new Usuario();

        /* insert de dados 
          $user -> setNome("eu");
          $user -> setSenha("123");
          $user -> setNivel("n");
          $user ->insert_user(); */

        /* Select individual 
          $dados = $user->find(1,'usuario');
          echo $dados->nome_usuario;
          echo $dados->senha_usuario;
          echo $dados->nivel_usuario;
         */

        /*
          $user->setNome("eu");
          $user->setSenha("123");
          $user->setNivel("a");
          $user ->update_user(2);
         */
        /* Select List
          $dados = $user->findAll('usuario');
          foreach ($dados as $mostrar) {
          echo $mostrar->nome_usuario;
          echo $mostrar->senha_usuario;
          echo $mostrar->nivel_usuario . '<br>';
          }
         */
        ?>
    </body>
</html>
