<!DOCTYPE html>

<!--
Pagina publica de cadastro
autor: Silvarney Henrique
-->

<html>
    <head>
        <meta charset="UTF-8">
        <title>Cadastro</title>
    </head>
    <body>
        <!-- Formulario para cadastro de usuario: envia os dados para a cadastrar_usuraio.php-->
        <form id="cadastro" name="cadastro" method="post" action="controle/cadastrar_usuario.php">
            <fieldset>
                <legend>Novo Usuário:</legend>
                <input type="text" name="nome" placeholder="seu nome"><br>
                <input type="text" name="senha" placeholder="sua senha"><br>
                <select name="nivel">
                    <option value="p">Usuario</option>
                    <option value="a">Administrador</option>
                </select>
                <input type="submit" name="submit" value="Salvar"><br>
            </fieldset>
        </form>
    </body>
</html>
