<!DOCTYPE html>
<!--
Area do Usuario
autor: Silvarney Henrique
-->

<?php
session_start(); //start a sessao iniciada na autenticação
require ("../controle/protecao.php"); //link para utilizacao das funcoes dentro de do arquivo "protecao"
protegerUsuario(); //funcao que testa se o usuario logado tem autorizacao para acessar essa pagina
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Pagina de Usuario</title>
    </head>
    <body>
        <h1>Bem Vindo, Usuário!</h1>
    </body>
</html>

<!--
Pagina inicial do administrador de dentro pasta "adm". 
-->