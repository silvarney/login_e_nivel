<?php

require_once '../classes/Usuario.php'; //link para utilizacao das funcoes dentro de do arquivo "protecao"

$nome_cad = isset($_POST['nome']) ? $_POST['nome'] : ''; // recebendo e validando dados vindos da pagina de cadastro
$senha_cad = isset($_POST['senha']) ? $_POST['senha'] : '';
$nivel_cad = isset($_POST['nivel']) ? $_POST['nivel'] : '';

$user = new Usuario(); //instanciando um novo usuario

$user->setNome($nome_cad); // setando valores 
$user->setSenha($senha_cad);
$user->setNivel($nivel_cad);
$user->insert_user(); //chamando a funcao que inseri os dados no banco

header("location:../index.php");