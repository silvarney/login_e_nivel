<?php

function protegerUsuario() { // funcao validar se o usuario tem acesso de administrador 
    if ($_SESSION["nivel"] != 'p') { // o nivel de acesso for diferente
        echo "<script>location.href='../index.php'</script>"; //o usuario sera redirecionado para a pagina de login
    }
}

function protegerAdm() { // funcao validar se o usuario tem acesso de usuario padrao
    if ($_SESSION["nivel"] != 'a') {
        echo "<script>location.href='../index.php'</script>";
    }
}


