<?php

require_once '../classes/Usuario.php'; //link para utilizacao das funcoes dentro de do arquivo "protecao"

$nome_login = isset($_POST['nome']) ? $_POST['nome'] : ''; // recebendo e validando dados vindos da pagina de login 
$senha_login = isset($_POST['senha']) ? $_POST['senha'] : ''; // recebendo e validando dados vindos da pagina de login

$user = new Usuario(); //instanciando um novo usuario
$usuario = $user->login($nome_login, $senha_login); //chamando a funcao "login" de dentro da classe usuario

if (isset($usuario)) { //se o usuario existir

    if ($usuario->nivel_usuario == 'a') { //se o nivel de acesso for "a" de administrador
        session_start(); // inicia a sessao
        $_SESSION['nome'] = $usuario->nome_usuario; //armazenando os dados vindo do banco dentro de variaveis de sessao
        $_SESSION['senha'] = $usuario->senha_usuario; //armazenando os dados vindo do banco dentro de variaveis de sessao
        $_SESSION['nivel'] = $usuario->nivel_usuario; //armazenando os dados vindo do banco dentro de variaveis de sessao
        header("location:../adm/index.php"); //redireciona o usuario para a pagina inicial do adminstrador
    } elseif ($usuario->nivel_usuario == 'p') { //se o nivel de acesso for "p" de usuario padrao
        session_start();
        $_SESSION['nome'] = $usuario->nome_usuario;
        $_SESSION['senha'] = $usuario->senha_usuario;
        $_SESSION['nivel'] = $usuario->nivel_usuario;
        header("location:../user/index.php"); //redireciona o usuario para a pagina inicial do adminstrador
    }
} elseif (!isset($usuario)) { //se o usuario não existir
    echo 'Nome ou senha errada!'; // mostra mensagem de erro
}

