<?php

// classe "Usuario", com todos os atributos/coluna que a tabela "usuario" contem.
require_once 'Crud.php';

class Usuario extends Crud {

    private $nome;
    private $senha;
    private $nivel;

    public function setNome($nome) { //inserindo valores
        $this->nome = $nome;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
    }

    public function setNivel($nivel) {
        $this->nivel = $nivel;
    }

    public function getNome() { // lendo valores
        return $this->nome;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function getNivel() {
        return $this->nivel;
    }

    public function insert_user() { //enviando valores para o banco de dados
        try {
            $sql = "INSERT INTO usuario (nome_usuario, senha_usuario, nivel_usuario) "
                    . "VALUES (:nome, :senha,:nivel)";
            $stmt = DB::prepare($sql);
            $stmt->bindParam(':nome', $this->nome);
            $stmt->bindParam(':senha', $this->senha);
            $stmt->bindParam(':nivel', $this->nivel);
            return $stmt->execute();
        } catch (Exception $ex) {
            echo 'Erro em inserir usuario <br>';
            $ex->getMessage();
        }
    }

    public function update_user($id) { // atualizando valores para o banco de dados
        try {
            $sql = "UPDATE usuario SET nome_usuario = :nome, senha_usuario = :senha, nivel_usuario = :nivel WHERE id_usuario = :id";
            $stmt = DB::prepare($sql);
            $stmt->bindParam(':nome', $this->nome);
            $stmt->bindParam(':senha', $this->senha);
            $stmt->bindParam(':nivel', $this->nivel);
            $stmt->bindParam(':id', $id);
            return $stmt->execute();
        } catch (Exception $ex) {
            echo 'Erro em update usuario <br>';
            $ex->getMessage();
        }
    }

    public function login($nome, $senha) { // checkando se nome e senha informados, constam no banco de dados
        try {

            $sql = "SELECT * FROM usuario WHERE nome_usuario = :nome AND senha_usuario = :senha";
            $stmt = DB::prepare($sql);
            $stmt->bindParam(':nome', $nome);
            $stmt->bindParam(':senha', $senha);
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                return $stmt->fetch(PDO::FETCH_OBJ);
            }
        } catch (Exception $ex) {
            echo 'Erro no login do usuario <br>';
            $ex->getMessage();
        }
    }

}
