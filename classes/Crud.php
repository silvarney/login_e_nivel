<?php

require_once '../banco/db.php'; //link para utilizacao das funcoes dentro de do arquivo "db"

abstract class Crud extends DB {

    public function find($id, $tabela) {
        $sql = "SELECT * FROM ".$tabela." WHERE id_".$tabela." = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    public function findAll($tabela) {
        $sql = "SELECT * FROM ".$tabela;
        $stmt = DB::prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function delete($id, $tabela) {
        $sql = "DELETE FROM ".$tabela." WHERE id_".$tabela." = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }

}
